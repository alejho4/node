var express = require("express");
var server = express();
var bodyParser = require("body-parser");



server.use(bodyParser.json());
//
{
//inicio

var model = {
};

model.clients = {};
model.reset = function(){
	this.clients = {}
}
model.addAppointment = function(name, date){
  if(this.clients[name]) return this.clients[name].push({status: 'pending', ...date})
  this.clients[name] = [{status: 'pending', ...date}]
}


model.attend = function(name,  date ){
	client = this.clients[name] 
	return client.forEach( child => child.date == date ? (child.status = 'attended') : null)
}
model.expire = function(name, date){
	client = this.clients[name] 
	return client.forEach( child => child.date == date ? (child.status = 'expired') : null)
}
model.cancel = function(name, date ){
	client = this.clients[name] 
	client.forEach( child => child.date == date ? (child.status = 'cancelled') : null)
	return this.clients[name]
}
model.erase = function(name, variable){
	if(variable.split('/').length > 1){ 
		this.clients[name] = this.clients[name].filter( child => child.date != variable)
		return this.clients[name]
    }
    else {
		this.clients[name] = this.clients[name].filter( child => child.status != variable)
		return this.clients[name]
	}
}

model.getAppointments = function(name, status){
		if(status) return this.clients[name].filter( child => child.status == status)
		return this.clients[name]
}

model.getClients = function(){
	return Object.keys(this.clients)
}



//-- fin
}
//




server.get('/api', (req, res) => {
	res.send(model.clients)
})


server.post('/api/Appointments', (req, res) => {
	
	const {client, appointment} = req.body

	if(req.body == undefined){
		res.status(400).send('the body must have a client property')	
	}
	if(!req.body.client){
		res.status(400).send('the body must have a client property')	
	}
	else if(typeof(req.body.client) != 'string') {
		res.status(400).send('client must be a string')
	}
	else {
		model.addAppointment( client, appointment)
		res.status(200).send(model.clients[client][0])
	}
	
	
})

server.get('/api/appointments/clients', (req, res) => {
	res.send(model.getClients())
})


server.get('/api/appointments/:name', (req, res) => {
	const { name } = req.params
	const { date, option } = req.query
	
	let client = model.clients[name]


	if(!client){
		res.status(400).send('the client does not exist')	
	}


	let citas = client.filter(child => child.date == date)
	let status = (option == 'attend' | option =='expire' || option == 'cancel')

	if(option == 'attend' && citas.length > 0){
		res.send(model.attend(name, date))
	}
	if(option == 'expire' && citas.length > 0){
		res.send(model.expire(name, date))
	}
	if(option == 'cancel' && citas.length > 0){
		//agregué filter[0] para que machee con el test
		res.send(model.cancel(name, date).filter( child => child.status == 'cancelled')[0])
	}
	if(!status){
		res.status(400).send('the option must be attend, expire or cancel')
	}
	if(citas.length == 0){
		res.status(400).send('the client does not have a appointment for that date')
	}

	res.status(400)
	
})

server.get('/api/appointments/:name/erase', (req, res) => {
	const { name } = req.params
	const { date } = req.query


	if(!model.clients[name]){
		res.status(400).send('the client does not exist')
	}

	let citas = model.getAppointments(name, date)
	model.erase(name, date)
	res.send(citas)

})

server.get('/api/appointments/getappointments/:name', (req, res) => {
	const { name } = req.params
	const { status } = req.query
	res.send(model.getAppointments(name, status))
})




server.listen(3000);
module.exports = { model, server };









